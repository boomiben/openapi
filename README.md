# README #

This repo contains a first foray into building Flow Services with openAPI.  Everything in here is probably broken.

### What is this repository for? ###

* It will contain the basic instructions for hosting the YAML files, so I don't need to always update the tenant assets.  I'm using tmux, but of course you can use screen or whatever.

### How do I get set up? ###

* Install Docker / ngrok (and configure auth token) / tmux / etc
* Launch an Apache docker container (update volume path as appropriate)
* * docker run -d --name openAPI -v /home/bdeboer/Repos/openapi/YAML:/usr/local/apache2/htdocs -p 8080:80 httpd:2.4
* Open a tmux session
* * tmux new -s ngrok
* * ngrok http --host-header=rewrite 8080
* * * CTL+B, D \#to detach
* * * tmux a -t ngrok \#to reattach
* Configure Flow openAPI service to read hosted YAML file


